package io.staggui.ui;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.staggui.api.ApiClient;
import io.staggui.api.exceptions.NonValidTokenException;
import io.staggui.api.models.*;
import io.staggui.scenes.LoginWindow;
import io.staggui.scenes.MarksWindow;
import io.staggui.tools.Config;
import io.staggui.ui.nodes.TimeTableItem;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Controller for the window.
 *
 * There should happened all the events and access to components.
 */
public class MainWindowController extends BorderPane {
    private static final File configFile = new File("config.json");
    private static final ObjectMapper mapper = new ObjectMapper();
    private final List<TimeTableItem> items = new ArrayList<>();
    private final Map<Integer, Pane> daysPanes = new HashMap<>();

    @FXML
    private MenuItem login;

    @FXML
    private GridPane table;

    @FXML
    private MenuBar menubar;

    /**
     * Function which is called from inside of generated fxml component
     *
     * Calls on button click action
     * @param event event
     */
    @FXML
    void onAuth(ActionEvent event) {
        LoginWindow.openAndCallOnSuccess(table.getScene().getWindow(), this::loadTimeTable);
    }


    /**
     * Function which is called from inside of generated fxml component
     *
     * Calls on button click action
     * @param event event
     */
    @FXML
    void onReload(ActionEvent event) {
        table.getChildren().removeAll(items);
        items.clear();
        for (var i : DayEnum.ordinals()) {
            daysPanes
                .get(i)
                .setVisible(true);
            table.getRowConstraints().get((i * 2) + 1).setMaxHeight(Double.MAX_VALUE);
            table.getRowConstraints().get((i * 2) + 2).setMaxHeight(Double.MAX_VALUE);
        }
        loadTimeTable();
    }

    /**
     * Function which is called from inside of generated fxml component
     *
     * Is called after all components are initialized
     */
    @FXML
    public void initialize() {
        BorderPane borderPane = new BorderPane();
        borderPane.setBackground(new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY)));
        borderPane.setBorder(new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderStroke.THIN)));

        table.add(borderPane, 1, 1, 15, 15);

        createDays();

        SwingUtilities.invokeLater(this::initEnvironment);

        menubar.useSystemMenuBarProperty().set(true);
    }

    /**
     * Function which is called from inside of generated fxml component
     *
     * Calls on button click action
     * @param event event
     */
    @FXML
    public void onMarks(ActionEvent event) {
        MarksWindow.open();
    }

    /**
     * Loads data from saved configuration file into all needed components
     */
    private void initEnvironment() {
        if(configFile.exists()) {
            try {
                Config c = mapper.readValue(configFile, Config.class);

                // Loads data to ApiClient for no need to login again if the tokens did not expire
                ApiClient.getInstance().loadDetails(c.getAuthDetails());

                // Gets the data and populate the grid with subjects
                populateTimeTable(c.getSubjects());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void createDays() {
        Background b = new Background(new BackgroundFill(Color.WHITE, CornerRadii.EMPTY, Insets.EMPTY));
        Border bor = new Border(new BorderStroke(Color.BLACK, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, BorderStroke.THIN));

        for (DayEnum d: DayEnum.values()) {
            Label l = new Label(d.toString());
            l.setFont(new Font(l.getFont().getName(), 24));

            BorderPane borderPane1 = new BorderPane();
            borderPane1.setBackground(b);
            borderPane1.setCenter(l);
            borderPane1.setBorder(bor);
            table.add(borderPane1, 0, (d.ordinal()*2)+1, 1, 2);
            daysPanes.put(d.ordinal(), borderPane1);
        }
    }


    private void loadTimeTable() {
        var currentDataTask = ApiClient
            .getInstance()
            .createCurrentAcademicInfoTask();

        currentDataTask.setOnSucceeded(workerStateEvent -> currentDataTask
            .getValue()
            .ifPresent(currentAcademicStateInfo -> {
                var term = currentAcademicStateInfo.getObdobi();
                var year = Integer.parseInt(currentAcademicStateInfo.getAkademRok());

                var timeTableTask = ApiClient
                    .getInstance()
                    .createTimeTableTask(term, year);

                timeTableTask.setOnSucceeded(workerStateEvent1 -> timeTableTask.getValue().ifPresent(timeTable -> parseTimeTable(timeTable, term)));

                timeTableTask.setOnCancelled(workerStateEvent1 -> {
                    if (timeTableTask.getException().getClass() == NonValidTokenException.class) {
                        LoginWindow.openAndCallOnSuccess(table.getScene().getWindow(), MainWindowController.this::loadTimeTable);
                    }
                });

                new Thread(timeTableTask).start();
            }));

        new Thread(currentDataTask).start();
    }

    /**
     * Take the list of subjects and map them to the GridView
     * @param parsedSubjects list of parsed subjects
     */
    private void populateTimeTable(List<Subject> parsedSubjects) {
        boolean[] collisions = new boolean[DayEnum.length]; // boolean array of colliding days which will be shown
        boolean[] show = new boolean[DayEnum.length]; // boolean array of days which will be shown

        for (int i = 0; i < DayEnum.length; i++) {
            // By default all columns are hidden (their max height is set to 0)
            table.getRowConstraints().get((i * 2) + 1).setMaxHeight(0);
            table.getRowConstraints().get((i * 2) + 2).setMaxHeight(0);

            // All showing arrays are by default set to false or rewritten to false
            collisions[i] = false;
            show[i] = false;
        }

        // Start mapping subjects to GridView
        for(Subject s : parsedSubjects) {
            //  TimeTableItem takes values from Subject and map it into graphical component
            TimeTableItem tti = new TimeTableItem(s);
            items.add(tti);

            // determination of if the whole day should be shown
            if(!show[s.getDenZkr().ordinal()]) {
                show[s.getDenZkr().ordinal()] = true;
            }

            // determination of if the colliding row should be shown
            if(!collisions[s.getDenZkr().ordinal()]) {
                collisions[s.getDenZkr().ordinal()] = s.isColliding();
            }

            tti.addToTimeTable(table);
        }

        // going throw showing arrays and setting visibility of rows
        for (int i = 0; i < DayEnum.length; i++) {
            if(show[i]) {
                table.getRowConstraints().get((i * 2) + 1).setMaxHeight(Double.MAX_VALUE);

                if(collisions[i]) {
                    table.getRowConstraints().get((i * 2) + 2).setMaxHeight(Double.MAX_VALUE);
                }
            }
        }
    }

    // function that parses RESP API data into correct array of wanted subjects
    private void parseTimeTable(TimeTable timeTable, Term term) {
        // requesting week of year to determine which week is now to show correct week for distance students
        var currentWeekOfYear = Calendar.getInstance().get(Calendar.WEEK_OF_YEAR);
        /*
         *  1. level = day array (0 - 6 -> PO - NE)
         *  2. level = subjects in the day
         */
        var parsed = new ArrayList<List<Subject>>();

        for (int i = 0; i < DayEnum.length; i++) {
            parsed.add(new ArrayList<>());
        }

        for (Subject subject : timeTable.getRozvrhovaAkce()) {
            // conditions that determine if is subject should not be accepted
            if(!subject.getSemestr().equals(term.name()) || subject.getHodinaOd() == 0 || !isEventAllowed(subject.getTypAkceZkr()) || (timeTable.getStudyType() == StudyType.PARTTIME && currentWeekOfYear != subject.getTydenOd()))
                continue;

            var dayIndex = subject.getDenZkr().ordinal();

            // detecting if is in the parsed array have any subject that is colliding
            var isColliding = parsed
                .get(dayIndex)
                .stream()
                .anyMatch(subject::isColliding);

            // set flag if the subject is colliding
            subject.setColliding(isColliding);

            // add subject to parsed array
            parsed.get(dayIndex).add(subject);
        }

        // this block take the 2D list and transform it into 1D list
        var subjects = parsed
            .stream()
            .flatMap(Collection::stream)
            .collect(Collectors.toList());

        populateTimeTable(subjects);

        try {
            // writes all data to config file, to be used in new instance of this app
            mapper.writerWithDefaultPrettyPrinter().writeValue(configFile, new Config(ApiClient.getInstance().getDetails(), subjects));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // filter that rejects any other event type except the which are entered
    private boolean isEventAllowed(String type) {
        switch (type.toLowerCase()) {
            case "př":
            case "cv":
            case "se":
                return true;
        }

        return false;
    }
}
