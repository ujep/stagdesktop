package io.staggui.ui.nodes;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class YearItem {
    private final int year;
    private final int grade;

    @Override
    public String toString() {
        return String.format("%s (%s - %s)", grade, year, year + 1);
    }
}
