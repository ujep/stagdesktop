package io.staggui;

import io.staggui.ui.MainWindowController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * This class os used because maven (package manager), dont like JavaFX "Application" extended in main class
 */
public class MainFXWrapper extends Application {

    public static void start(String[] args) {
        launch(args);
    }

    /**
     * Function which is called when primary window is creating
     * @param primaryStage primary window
     * @throws Exception exception to prevent of fall down whole program
     */
    @Override
    public void start(Stage primaryStage) throws Exception {
        primaryStage.setScene(new Scene(FXMLLoader.load(getClass().getClassLoader().getResource("mainWindow.fxml"))));
        primaryStage.setTitle("STAG Desktop");
        primaryStage.show();
    }
}
