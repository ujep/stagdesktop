package io.staggui.api.exceptions;

public class NonValidTokenException extends Exception {
    public NonValidTokenException(String message) {
        super(message);
    }
}
