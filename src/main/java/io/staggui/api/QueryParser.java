package io.staggui.api;


import java.util.HashMap;
import java.util.Map;

/**
 * Static class for parsing url params
 */
public class QueryParser {
    /**
     * Function that takes GET request params and tries to parse then into map
     * @param quarryParams quarry params as string
     * @return Map of params
     */
    public static Map<String, String> parseParams(String quarryParams) {
        Map<String, String> params = new HashMap<>();

        for (String s : quarryParams.split("&")) {
            String[] s1 = s.split("=");

            params.put(s1[0], s1[1]);
        }

        return params;
    }

    /**
     * Function that takes full GET url and tries to parse it into params map
     * @param url string url
     * @return Map of params
     */
    public static Map<String, String> parseUrl(String url) {
        if(url.contains("?")) {
            return parseParams(url.split("[?]")[1]);
        }

        return new HashMap<>();
    }
}
