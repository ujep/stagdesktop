package io.staggui.api;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Builder class for making string url with params
 */
public class URLBuilder {
    private final Map<String, String> params = new HashMap<>();
    private final String path;

    private URLBuilder(String path) {
        this.path = path;
    }

    public static URLBuilder create(String path) {
        return new URLBuilder(path);
    }

    /**
     * Add param to builder
     * @param key key
     * @param value value
     * @return returns builder class
     */
    public URLBuilder add(String key, Object value) {
        params.put(key, value.toString());

        return this;
    }

    /**
     * Build string url with entered params
     * @return returns string url with entered params
     */
    public String build() {
        var paramsStringArray = params
                .entrySet()
                .stream()
                .map(this::mapEntryToString)
                .collect(Collectors.joining("&"));

        return path + "?" + paramsStringArray;
    }

    /**
     * Convert key value pair into url request form
     * @param e key value pair
     * @return string of url request formed data
     */
    private String mapEntryToString(Map.Entry<String, String> e) {
        return String.format("%s=%s", e.getKey(), e.getValue());
    }
}
