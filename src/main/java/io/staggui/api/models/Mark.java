package io.staggui.api.models;

import lombok.Data;

@Data
public class Mark{
    public String katedra;
    public String zkratka;
    public String rok;
    public String semestr;
    public String os_cislo;
    public String jmeno;
    public String prijmeni;
    public String titul;
    public String nesplnene_prerekvizity;
    public String zk_typ_hodnoceni;
    public String zk_datum;
    public String zk_hodnoceni;
    public String zk_body;
    public String zk_pokus;
    public String zk_ucit_idno;
    public String zk_jazyk;
    public String zk_ucit_jmeno;
    public Object zppzk_typ_hodnoceni;
    public String zppzk_datum;
    public String zppzk_hodnoceni;
    public String zppzk_pokus;
    public String zppzk_ucit_idno;
    public String zppzk_ucit_jmeno;
    public String zppzk_uznan;
}
