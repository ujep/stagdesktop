package io.staggui.api.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Marks {
    @JsonProperty("student_na_predmetu")
    private List<Mark> studentNaPredmetu;
}
