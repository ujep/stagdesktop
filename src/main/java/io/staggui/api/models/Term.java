package io.staggui.api.models;

import lombok.Getter;

@Getter
public enum Term {
    LS("Letní semestr"), ZS("Zimní semestr");

    private final String description;

    Term(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return description;
    }
}
