package io.staggui.tools;

import io.staggui.api.AuthDetails;
import io.staggui.api.models.Subject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Config {
    private AuthDetails authDetails;
    private List<Subject> subjects;
}
