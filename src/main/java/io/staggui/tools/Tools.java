package io.staggui.tools;

import io.staggui.api.ApiClient;
import io.staggui.api.models.Mark;
import io.staggui.ui.nodes.YearItem;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Label;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Tools {
    public static ObservableList<YearItem> generateYears() {
        var items = new ArrayList<YearItem>();

        int currentGrade = Integer.parseInt(ApiClient.getInstance().getStudent().getRocnik());
        int currentYear = LocalDate.now().getYear();

        for (int i = 0; i < currentGrade; i++) {
            items.add(new YearItem(currentYear - currentGrade + i, i + 1));
        }

        return FXCollections.observableList(items);
    }

    public static List<Label> createMarkItems(Mark mark) {
        if(mark.getZk_hodnoceni().equalsIgnoreCase("s") || mark.getZk_hodnoceni().equalsIgnoreCase("n")) {
            return List.of(
                new Label(mark.getKatedra() + "/" + mark.getZkratka()),
                new Label(),
                new Label(),
                new Label(),
                new Label(),
                new Label(mark.getZk_hodnoceni()),
                new Label(mark.getZk_datum()),
                new Label(mark.getZk_pokus()),
                new Label(mark.getZk_ucit_jmeno())
            );
        }

        return List.of(
            new Label(mark.getKatedra() + "/" + mark.getZkratka()),
            new Label(mark.getZk_hodnoceni()),
            new Label(mark.getZk_datum()),
            new Label(mark.getZk_pokus()),
            new Label(mark.getZk_ucit_jmeno()),
            new Label(mark.getZppzk_hodnoceni()),
            new Label(mark.getZppzk_datum()),
            new Label(mark.getZppzk_pokus()),
            new Label(mark.getZppzk_ucit_jmeno())
        );
    }
}
